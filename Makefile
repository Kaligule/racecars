output/frame_001.png: src/main.rs Cargo.toml
	cargo run

output/movie.mp4: output/frame_001.png
	ffmpeg -f image2 -r 25 -i output/frame_%03d.png -c:v libx264 -pix_fmt yuv420p -y output/movie.mp4

output/movie.gif: output/movie.mp4
	ffmpeg -i output/movie.mp4 -y output/movie.gif

# To get life updates you can try this:
# mpv "mf://output/*.png" -mf-fps 25 --loop-playlist --really-quiet  & ls src/*.rs | entr -c cargo run
