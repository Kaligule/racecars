with import <nixpkgs> {};

stdenv.mkDerivation {
  name = "rust-development-derivation";
  buildInputs = [
    cargo
    rustc
    gcc
    ffmpeg # for combining frames
  ];
}
