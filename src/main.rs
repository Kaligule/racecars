extern crate image;

use crate::image::GenericImageView;
use crate::image::GenericImage;
use image::Rgba;
use std::fs;

const TAU: f32 = 2. * std::f32::consts::PI;

// colors
const SENSOR_COLOR: Rgba<u8> = image::Rgba([255, 0, 0, 255]);
const WALL_COLOR: Rgba<u8> = image::Rgba([255, 255, 255, 255]); // white
const DEAD_CAR_COLOR: Rgba<u8> = image::Rgba([100, 100, 100, 255]);
const START_POSITION_COLOR: Rgba<u8> = image::Rgba([0, 255, 0, 255]); // green
const START_DIRECTION_COLOR: Rgba<u8> = image::Rgba([0, 0, 255, 255]); // blue
const DRIVING_CAR_COLOR: Rgba<u8> = image::Rgba([255, 255, 0, 255]); // yellow

const CAR_LENGTH: f32 = 8.;
const CAR_WIDTH: f32 = 5.;

// output length
const RACE_FRAMES: u32 = 300;
const AFTERLIFE_FRAMES: u32 = 20;

#[derive(Copy, Clone, Debug)]
struct Car {
    alive: bool,
    color: Rgba<u8>,
    x: f32,
    y: f32,
    speed: f32,
    min_speed: f32,
    max_speed: f32,
    direction: f32,
    // 3 sensors: left front, front, right front
    sensor_direction: (f32, f32, f32), // relative_to_the_car
    sensed_distance: (f32, f32, f32),

    steering_factor: f32,
    throteling_factor: f32
}


fn new_car(starting_point: (f32, f32), direction: f32) -> Car {
    Car {
        alive: true,
        color: DRIVING_CAR_COLOR,
        x: starting_point.0,
        y: starting_point.1,
        speed:0.,
        min_speed:0.5,
        max_speed:10.,
        direction: direction,
        sensor_direction: (TAU * -0.125, 0., TAU * 0.125),
        sensed_distance: (std::f32::INFINITY, std::f32::INFINITY, std::f32::INFINITY),
        steering_factor: 0.03,
        throteling_factor: 0.05
    }
}

impl Car {

    fn step(self: &mut Car, map: &mut Map){
        if self.alive {
            // steer
            self.sense(map);
            self.direction += self.steering_factor * (self.sensed_distance.2 - self.sensed_distance.0);
            self.speed = self.throteling_factor * self.sensed_distance.1 * self.max_speed;
            if self.speed < self.min_speed {
                self.speed = self.min_speed;
            } else if self.max_speed < self.speed {
                self.speed = self.max_speed;
            }
            // drive
            self.x += self.speed * self.direction.cos();
            self.y += self.speed * -self.direction.sin();
            // crash
            if self.sensed_distance.1 == 0.0 {
                self.alive = false;
            }
        }
    }

    fn sense(self: &mut Car, map: &mut Map){
        // absolute directions of sensors of this car
        let sensor_dir_0 = self.direction + self.sensor_direction.0;
        let sensor_dir_1 = self.direction + self.sensor_direction.1;
        let sensor_dir_2 = self.direction + self.sensor_direction.2;

        self.sensed_distance.0 = self.sense_one_direction(sensor_dir_0, map);
        self.sensed_distance.1 = self.sense_one_direction(sensor_dir_1, map);
        self.sensed_distance.2 = self.sense_one_direction(sensor_dir_2, map);
    }

    fn sense_one_direction(self: &mut Car, direction: f32, map: &mut Map) -> f32 {
        // direction is absolute
        let delta_x: f32 = direction.cos();
        let delta_y: f32 = -direction.sin();
        let mut point_x: f32 = self.x;
        let mut point_y: f32 = self.y;
        let mut point_x_rounded: u32 = self.x.round() as u32;
        let mut point_y_rounded: u32 = self.y.round() as u32;
        while map.collision_map.in_bounds(point_x_rounded, point_y_rounded)
            && map.spot_is_free(point_x_rounded, point_y_rounded) {
                // mark point on map
                map.collision_map.put_pixel(point_x_rounded, point_y_rounded, SENSOR_COLOR);

                point_x += delta_x;
                point_y += delta_y;
                point_x_rounded = point_x.round() as u32;
                point_y_rounded = point_y.round() as u32;
            }
        let distance: f32 = ((point_x - self.x).powi(2) + (point_y - self.y).powi(2)).sqrt();
        return distance
    }
}


#[derive(Clone)]
struct Map {
    collision_map: image::DynamicImage,
    overlay: image::DynamicImage,
}

impl Map {

    fn starting_point_and_direction(&self) -> ((f32, f32), f32) {
        let mut starting_point: Option<(u32, u32)> = None;
        let mut direction_point: Option<(u32, u32)> = None;
        for p in self.collision_map.pixels() {
            match p {
                (x, y, START_POSITION_COLOR) => starting_point = Some((x, y)),
                (x, y, START_DIRECTION_COLOR) => direction_point = Some((x, y)),
                _ => ()
            }
        }
        let starting_point: (u32, u32) = starting_point.unwrap();
        let direction_point: (u32, u32) = direction_point.unwrap();
        let starting_point: (f32, f32) = (starting_point.0 as f32, starting_point.1 as f32);
        let direction_point: (f32, f32) = (direction_point.0 as f32, direction_point.1 as f32);
        let direction: f32 = ((starting_point.1-direction_point.1) as f32 /
                              (starting_point.0-direction_point.0) as f32).tan();

        (starting_point, direction)
    }

    fn overlay_scale(&self) -> f32 {
        let (overlay_width, _overlay_height) = self.overlay.dimensions();
        let (collision_map_width, _collision_map_height) = self.collision_map.dimensions();
        (overlay_width as f32) / (collision_map_width as f32)
    }

    fn draw_cars(&self, cars: &[&mut Car]) -> image::DynamicImage {
        let mut image = self.overlay.clone();
        for car in cars {
            draw_car(&mut image, self.overlay_scale(), car);
        }
        image
    }

    fn spot_is_free(&self, x: u32, y: u32) -> bool {
        let color_at_spot: Rgba<u8> = self.collision_map.get_pixel(x,y);
        match color_at_spot {
            WALL_COLOR => false,
            _ => true
        }
    }
}

fn update_all_cars(cars: &mut [&mut Car], map: &mut Map) {
    for car in cars {
        car.step(map)
    }
}

fn main () {
    let output_dir = "./output";
    for entry in fs::read_dir(output_dir).unwrap() {
        let path_to_file = entry.unwrap().path();
        fs::remove_file(path_to_file).expect("Could not remove file");
    }

    let map = Map{
        collision_map: image::open("src/loop.png").unwrap(),
        overlay: image::open("src/loopoverlay.png").unwrap(),
    };
    let (starting_point, starting_direction) = map.starting_point_and_direction();

    let mut test_car = new_car(starting_point, starting_direction);
    let mut cars: Vec<&mut Car> = vec![&mut test_car];

    for frame_number in 1..RACE_FRAMES {
        let mut map = map.clone();
        update_all_cars(&mut cars, &mut map);
        let map_with_cars = map.draw_cars(&cars);

        let frame_name = format!("output/frame_{:03}.png", frame_number);
        map_with_cars.save(frame_name).unwrap();
        if !cars.iter().any(|c| c.alive) {
            for afterlive_frame_number in frame_number+1..frame_number+1+AFTERLIFE_FRAMES {
                let frame_name = format!("output/frame_{:03}.png", afterlive_frame_number);
                map_with_cars.save(frame_name).unwrap();
            }
            break;
        }

    }
    println!("Done");
}


fn draw_car(img: &mut image::DynamicImage, scale: f32, car: &Car) {
    let color = if car.alive {car.color} else {DEAD_CAR_COLOR};

    let car_length_scaled      = (CAR_LENGTH * scale     ) as i32;
    let car_width_scaled_half  = (CAR_WIDTH  * scale / 2.) as i32;
    for l in -car_length_scaled..0 {
        let l = l as f32;
        for w in -car_width_scaled_half..car_width_scaled_half {
            let w = w as f32;
            let pixel_position_x: f32 = (car.x * scale) + w*car.direction.sin() + l*car.direction.cos();
            let pixel_position_y: f32 = (car.y * scale) + w*car.direction.cos() - l*car.direction.sin();
            let pixel_position_x: u32 = pixel_position_x.round() as u32;
            let pixel_position_y: u32 = pixel_position_y.round() as u32;
            if img.in_bounds(pixel_position_x, pixel_position_y) {
                img.put_pixel(pixel_position_x, pixel_position_y, color);
            }
        }
    }
}
