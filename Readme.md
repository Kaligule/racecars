# racecars

A simple simulation of cars driving a racetrack.
This is a reimplementation of [the code](https://gist.github.com/skeeto/da7b2ac95730aa767c8faf8ec309815c) referenced in [this inspiring blogpost](https://nullprogram.com/blog/2020/11/24/).
It is also a learning project to get my rust-skills up. Therefore I am especially thankfully for constructive feedback (via issues, mail or direct messages).
I also wrote a [blogpost](https://schauderbasis.de/posts/fun_errors/) about this code.

To start make sure you have the rust toolchain and ffmpeg installed (or just start a `nix-shell`) and go:

```shell
make output/movie.mp4
```
